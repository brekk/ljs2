## Release History

-   **1.0.0** — _2016-09-19_ — Initial release
    -   forked off of [ljs](https://github.com/phadej/ljs) 0.3.2
-   **2.0.0** — _2019-01-10_ — Major rewrite using sywac
