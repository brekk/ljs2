#!/usr/bin/env node
'use strict';

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var ramda = require('ramda');
var esprima = require('esprima');
var fs = _interopDefault(require('fs'));
var path = _interopDefault(require('path'));
var glob = _interopDefault(require('glob'));
var isGlob = _interopDefault(require('is-glob'));
var assert = _interopDefault(require('assert'));
var sywac = _interopDefault(require('sywac'));
var F = _interopDefault(require('fluture'));

var range = ramda.curry(function _range(fromStart, toEnd, comment) {
  if (!comment || !comment.range) {
    throw new ReferenceError("Expected to be able to access comment.range.")
  }
  var output = comment.range[0] >= fromStart && comment.range[1] <= toEnd;
  return output
});
var addWhitespace = ramda.curry(function _addWhitespace(
  raw,
  syntax,
  tokens,
  fromStart,
  toEnd
) {
  if (!syntax || !syntax.comments) {
    throw new ReferenceError("Expected to be able to access syntax.comments.")
  }
  var ws;
  var commentFilter = ramda.filter(range(fromStart, toEnd));
  var comments = commentFilter(syntax.comments);
  ramda.forEach(function _processComment(c) {
    if (c.range[0] !== fromStart) {
      ws = raw.substr(fromStart, c.range[0] - fromStart);
      tokens.push({
        type: "Whitespace",
        value: ws,
        range: [fromStart, c.range[0]]
      });
    }
    tokens.push({
      type: "Comment",
      value: c,
      range: c.range
    });
    fromStart = c.range[1];
  }, comments);
  ws = raw.substr(fromStart, toEnd - fromStart);
  tokens.push({
    type: "Whitespace",
    value: ws,
    range: [fromStart, toEnd]
  });
});

var lex = ramda.curry(function (options, raw) {
  var syntax = esprima.parse(
    raw,
    Object.assign({}, options, {
      tokens: true,
      loc: true,
      range: true,
      comment: true
    })
  );
  var tokens = [];
  var currRange = 0;
  syntax.tokens.forEach(function (token) {
    if (token.range[0] !== currRange) {
      addWhitespace(raw, syntax, tokens, currRange, token.range[0]);
    }
    tokens.push(token);
    currRange = token.range[1];
  });
  addWhitespace(raw, syntax, tokens, currRange, raw.length);
  return tokens
});

var magicIndicator = " =>";
var codeFenceOpen = "\n```js\n";
var codeFenceClose = "\n```\n\n";
var CODE = "code";
var TEXT = "text";
var EMPTY = "";
var STAR = "*";
var NEWLINE = "\n";
var tokenTypes = Object.freeze({
  EOF: "EOF",
  line: "Line",
  plain: "Plain",
  comment: "Comment",
  block: "Block"
});
var withTrailingAndOptionalWhitespace = function (x) { return new RegExp(("^\\s*" + x + "\\s+(.*?)\\s*$")); };
var regex = {
  plain: withTrailingAndOptionalWhitespace("plain"),
  include: withTrailingAndOptionalWhitespace("include"),
  ignore: withTrailingAndOptionalWhitespace("#"),
  whitespaceEnd: /^\s*$/,
  whitespace: /^(\s*)/,
  newline: /\n/,
  strippable: /^(?:\s*\n)+/,
  strippable2: /[\s\n]*$/,
  EOF: /\n+$/,
  shebang: /^#!\/[^\n]*\n/
};

var isWhitespace = function (x) { return regex.whitespaceEnd.test(x); };
var stripShebang = function (x) {
  var match = x.match(regex.shebang);
  return match ? x.substr(match[0].length) : x
};

var fileDirective = function _fileDirective(
  filename,
  value,
  comparison,
  callback
) {
  var match = value.match(comparison);
  if (match) {
    var directivePattern = match[1];
    var globPattern = path.join(path.dirname(filename), directivePattern);
    var isGlobby = isGlob(globPattern);
    var files = isGlobby ? glob.sync(globPattern) : [globPattern];
    if (files.length === 0 || (!isGlobby && !fs.existsSync(globPattern))) {
      throw new Error(directivePattern + " doesn't match any files")
    }
    files.forEach(callback);
    return true
  } else {
    return false
  }
};

var commentHasMagicIndicator = function (magic, t) {
  if (t && t.type && t.value && t.value.value) {
    return (
      t.type === tokenTypes.comment &&
      t.value.type === tokenTypes.line &&
      t.value.value.slice(0, magic.length) === magic
    )
  }
  return false
};
var getTokens = function (magic, filename) {
  var source = fs.readFileSync(filename, "utf8");
  var raw = stripShebang(source);
  var tokens = lex({ sourceType: "module" }, raw);
  var resTokens = [];
  ramda.forEach(function (t) {
    var r;
    if (commentHasMagicIndicator(magic, t)) {
      var value = t.value.value.substr(magic.length + 1);
      r = fileDirective(filename, value, regex.plain, function (name) {
        resTokens.push({
          type: tokenTypes.plain,
          value: fs.readFileSync(name).toString()
        });
      });
      if (r) {
        return
      }
      r = fileDirective(filename, value, regex.include, function (name) {
        resTokens = resTokens.concat(getTokens(magic, name));
      });
      if (r) {
        return
      }
      assert(false, "unknown directive: " + value);
    } else if (regex.ignore.test(t.value.value)) {
      return
    } else {
      t.raw = raw.substr(t.range[0], t.range[1] - t.range[0]);
      resTokens.push(t);
    }
  }, tokens);
  resTokens.push({
    type: tokenTypes.EOF,
    value: EMPTY
  });
  return resTokens
};
var processLine = ramda.curry(function (indent, line) {
  if (line.indexOf(indent) === 0) {
    return line.replace(indent, EMPTY)
  } else if (isWhitespace(line)) {
    return EMPTY
  } else {
    return line
  }
});
var firstLine = function (lines) { return ramda.find(function (l) { return !isWhitespace(l); }, lines); };
var getIndent = function (lines) {
  var first = firstLine(lines);
  return first ? regex.whitespace.exec(first)[1] : EMPTY
};
var unindent = function (value) {
  var lines = value.split(regex.newline);
  var indent = getIndent(lines);
  while (lines[0] !== undefined && isWhitespace(lines[0])) {
    lines.shift();
  }
  return ramda.map(processLine(indent), lines).join(NEWLINE) + NEWLINE
};
var typeIs = ramda.curry(function (x, ref) {
  var type = ref.type;
  return type === x;
});
var isPlain = typeIs(tokenTypes.plain);
var isEOF = typeIs(tokenTypes.EOF);
var isCommentStart = function (w, x) { return typeIs(tokenTypes.comment, x) &&
  x.value &&
  x.value.type === tokenTypes.block &&
  x.value.value &&
  x.value.value.indexOf(w) === 0; };
var literate = function (filename, opts) {
  if ( opts === void 0 ) opts = {};
  var code = opts.code; if ( code === void 0 ) code = false;
  var codeOpen = opts["format-code-block-open"]; if ( codeOpen === void 0 ) codeOpen = codeFenceOpen;
  var codeClose = opts["format-code-block-close"]; if ( codeClose === void 0 ) codeClose = codeFenceClose;
  var formatHeading = opts["format-heading"]; if ( formatHeading === void 0 ) formatHeading = STAR;
  var formatMagic = opts["format-magic"]; if ( formatMagic === void 0 ) formatMagic = magicIndicator;
  var tokens = getTokens(formatMagic, filename);
  var state = CODE;
  var content = EMPTY;
  var codeBuffer = EMPTY;
  var appendCode = function () {
    if (state === CODE) {
      state = TEXT;
      if (!isWhitespace(codeBuffer)) {
        content +=
          codeOpen +
          codeBuffer
            .replace(regex.strippable, EMPTY)
            .replace(regex.strippable2, EMPTY) +
          codeClose;
      }
    }
  };
  var appendText = function (value) {
    if (content === EMPTY) {
      content = value;
    } else {
      content += NEWLINE + value;
    }
  };
  ramda.forEach(function processToken(t) {
    if (isPlain(t)) {
      appendCode();
      appendText(t.value);
    } else if (isEOF(t)) {
      appendCode();
      appendText(EMPTY);
    } else if (isCommentStart(formatHeading, t)) {
      appendCode();
      var comment = t.value;
      var value = comment.value.slice(
        comment.value.indexOf(formatHeading) + formatHeading.length
      );
      appendText(unindent(value));
    } else if (code) {
      if (state !== CODE) {
        state = CODE;
        codeBuffer = EMPTY;
      }
      codeBuffer += t.raw;
    }
  }, tokens);
  appendCode();
  content = content.replace(regex.EOF, NEWLINE);
  return content
};

var ascii = "\n      :::       ::::::::::: ::::::::   ::::::::\n     :+:           :+:    :+:    :+: :+:    :+:\n    +:+           +:+    +:+              +:+\n   +#+           +#+    +#++:++#++     +#+\n  +#+           +#+           +#+   +#+\n #+#       #+# #+#    #+#    #+#  #+#\n########## #####      ########  ##########\n";
var defaultParamWorkaround = {
  fm: magicIndicator,
  "format-magic": magicIndicator,
  fh: STAR,
  "format-heading": STAR,
  fo: codeFenceOpen,
  "format-code-block-open": codeFenceOpen,
  fc: codeFenceClose,
  "format-code-block-close": codeFenceClose
};
var ljs2 = function () {
  var cli = new F(function (bad, good) {
    sywac
      .preface(ascii, "a tool for working with literate JS")
      .showHelpByDefault()
      .help("-h, --help")
      .version("-v, --version")
      .positional("[input:string]", {
        paramsDesc: "The input file",
        mustExist: false
      })
      .string("-o, --output", { desc: "An output file" })
      .boolean("-C, --no-code", {
        desc: "Don't include code in the output file",
        defaultValue: false
      })
      .boolean("-p, --print-config", {
        desc: "Print the existing configuration and exit"
      })
      .boolean("-M, --no-meld", {
        desc: "Don't meld consecutive line-breaks",
        defaultValue: false
      })
      .string("-fm, --format-magic <string>", {
        desc: "Magic directive indicator (default: \" =>\")"
      })
      .string("-fh, --format-heading <string>", {
        desc: ("Delimiter for preserved comment blocks (default: \"" + STAR + "\")")
      })
      .string("-fo, --format-code-block-open <string>", {
        desc: 'Open code block string (default: "```js")'
      })
      .string("-fc, --format-code-block-close <string>", {
        desc: 'Close code block string (default: "```")'
      })
      .outputSettings({ maxWidth: 75 })
      .parseAndExit()
      .catch(bad)
      .then(good);
  });
  return cli.map(function (program) {
    var config = Object.entries(program).reduce(function (o, ref) {
      var obj;
      var k = ref[0];
      var v = ref[1];
      var out = Object.assign(o, ( obj = {}, obj[k] = (v === false || v === undefined) && defaultParamWorkaround[k]
            ? defaultParamWorkaround[k]
            : v, obj ));
      return out
    }, {});
    var output = config.output;
    var noMeld = config["no-meld"];
    var noCode = config["no-code"];
    var printConfig = config["print-config"];
    var filename = config.input;
    if (printConfig) { return console.log(config) }
    config.code = !noCode;
    var litContents;
    try {
      litContents = literate(filename, config);
    } catch (e) {
      process.stderr.write("Error while literating -- " + e.message);
      return 1
    }
    if (!noMeld) {
      litContents = litContents.replace(/\n\n+/g, "\n\n");
    }
    if (output) {
      fs.writeFileSync(output, litContents, "utf8");
    } else {
      process.stdout.write(litContents);
    }
  })
};

var I = function (x) { return x; };
ljs2().fork(I, I);
