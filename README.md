![ljs2](https://cdn.rawgit.com/brekk/ljs2/8a107c0/logo.svg)

```
      :::       ::::::::::: ::::::::   ::::::::
     :+:           :+:    :+:    :+: :+:    :+:
    +:+           +:+    +:+              +:+
   +#+           +#+    +#++:++#++     +#+
  +#+           +#+           +#+   +#+
 #+#       #+# #+#    #+#    #+#  #+#
########## #####      ########  ##########
```

> Generate docs from your source

## Command Line

If `ljs2` is installed globally, you can use `ljs2` command line tool to process your literate javascript files

You can use npx to proxy commands to ljs2 directly, e.g. `npx ljs2 input.js`

Otherwise, if you want it installed as a standalone, use `yarn global add ljs2` or `npm i -g ljs2`

```sh
$ ljs2 input-file.js -o output-file.md
$ ljs2 --help
```

*This file* is generated from ljs2's source code! [View it here](https://gitlab.com/brekk/ljs2/blob/master/src/ljs2.js)

## Usage

The basic idea with ljs2 is to prefix literate comments with the `/**` [literate comment block indicator](https://gitlab.com/brekk/ljs2/blob/master/ljs2.js#L113) (note the additional asterisk). Literate comments are then visible in the markdown document.

Additionally, individual, non-literate comments can use the magic directive: " =>" directly after the comment to either:

* via the `include` directive, include other literate code files: `// => include other-literate-file.js`
* via the `plain` directive, include other markdown files as included comments: `// => plain other-file.md`

## Configuration

Many of ljs2's behavior can be customized via flags. Please see `ljs2 --help` for more information, or [peruse the tests](https://gitlab.com/brekk/ljs2/blob/master/src/ljs2.spec.js) and their [fixtures](https://gitlab.com/brekk/ljs2/blob/master/src/fixtures)
 

## Release History

-   **1.0.0** — _2016-09-19_ — Initial release
    -   forked off of [ljs](https://github.com/phadej/ljs) 0.3.2
-   **2.0.0** — _2019-01-10_ — Major rewrite using sywac

# Contributors

* [phadej](https://github.com/phadej) - Oleg Grenrus
* [brekk](https://brekk.is) - Brekk Bockrath

## Related work

This tool can be used to do literate programming!
[Docco](http://jashkenas.github.io/docco/) is similar tool, however _ljs2_ is markup-language-agnostic.

The MIT License (MIT)

* Copyright (c) 2016 Brekk Bockrath
* Copyright (c) 2013, 2014 [Oleg Grenrus](https://github.com/phadej/ljs/blob/master/LICENSE)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
