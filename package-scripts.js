const utils = require("nps-utils")
const { concurrent, series } = utils
const { nps: npsAll } = concurrent

// const seriesNPS = (...x) => `nps ` + x.join(` && nps `)
const sd = (script, description) =>
  description ? { script, description } : script

const SKIP_DEPCHECK_FOR = [
  `@babel/cli`,
  `@babel/core`,
  `@babel/plugin-transform-destructuring`,
  `@babel/preset-env`,
  `babel-core`,
  `babel-jest`,
  `clinton`,
  `depcheck`,
  `documentation`,
  `docusaurus`,
  `husky`,
  `jest`,
  `lodash`, // lodash is used in a fixture still
  `prettier-eslint`,
  `rollup`
]
const meta = `./ljs2.js src/ljs2.js -o README.md --no-code`

module.exports = {
  scripts: {
    meta: sd(meta, `run ljs2 on its own source, to generate the README`),
    dependencies: sd(
      `depcheck --specials=bin,eslint,babel --ignores=${SKIP_DEPCHECK_FOR}`,
      `check dependencies`
    ),
    readme: sd(
      `echo 'documentation readme -s "API" src/**.js'`,
      `regenerate the readme`
    ),
    lint: {
      description: `lint both the js and the jsdoc`,
      script: npsAll(`lint.src`, `lint.jsdoc`, `lint.project`),
      src: sd(`eslint src/*.js --env jest --fix`, `lint js files`),
      jsdoc: sd(
        `documentation lint src/*/*.js,-src/fixtures/*.js`,
        `lint jsdoc in files`
      ),
      project: sd(`clinton --no-inherit`, `lint project using clinton`)
    },
    test: sd(
      `NODE_ENV=test jest --verbose --coverage`,
      `run all tests with coverage`
    ),
    testWatch: sd(
      `NODE_ENV=test jest --verbose --coverage --watchAll`,
      `run all tests with watcher`
    ),
    docs: {
      description: `auto regen the docs`,
      script: `echo 'documentation'`, // `documentation build src/**.js -f html -o docs`,
      serve: sd(`documentation serve src/**.js`, `serve the documentation`)
    },
    build: sd(
      series(`echo "you mean bundle!"`, `nps bundle`),
      `'cause I definitely do.`
    ),
    bundle: sd(
      series(`rollup -c rollup.config.js`, `chmod +x ljs2.js`),
      `generate bundles`
    ),
    regenerate: {
      readme: sd(
        `./snang.js --help > README.md && echo '## API' >> README.md`,
        `regenerate README from help text`
      ),
      help: sd(
        series(
          "echo '/* eslint-disable max-len */\nexport const HELP = `' > src/help.js",
          meta,
          "echo '`\n/* eslint-enable max-len */' >> src/help.js"
        ),
        "regenerate help from readme"
      )
    },
    care: sd(
      series(`nps bundle`, npsAll(`lint`, `test`, `dependencies`, `meta`)),
      `run all the things`
    ),
    generate: `nps bundle`
  }
}
