import fs from "fs"
import sywac from "sywac"
import F from "fluture"
import literate from "./literate"
import {
  STAR,
  magicIndicator,
  codeFenceOpen,
  codeFenceClose
} from "./constants"
const ascii = `
      :::       ::::::::::: ::::::::   ::::::::
     :+:           :+:    :+:    :+: :+:    :+:
    +:+           +:+    +:+              +:+
   +#+           +#+    +#++:++#++     +#+
  +#+           +#+           +#+   +#+
 #+#       #+# #+#    #+#    #+#  #+#
########## #####      ########  ##########
`
const defaultParamWorkaround = {
  fm: magicIndicator,
  "format-magic": magicIndicator,
  fh: STAR,
  "format-heading": STAR,
  fo: codeFenceOpen,
  "format-code-block-open": codeFenceOpen,
  fc: codeFenceClose,
  "format-code-block-close": codeFenceClose
}
const ljs2 = () => {
  const cli = new F((bad, good) => {
    sywac
      .preface(ascii, "a tool for working with literate JS")
      .showHelpByDefault()
      .help("-h, --help")
      .version("-v, --version")
      // .string("-i, --input", { desc: "An input file" })
      .positional("[input:string]", {
        paramsDesc: "The input file",
        mustExist: false
      })
      .string("-o, --output", { desc: "An output file" })
      .boolean("-C, --no-code", {
        desc: "Don't include code in the output file",
        defaultValue: false
      })
      .boolean("-p, --print-config", {
        desc: "Print the existing configuration and exit"
      })
      .boolean("-M, --no-meld", {
        desc: "Don't meld consecutive line-breaks",
        defaultValue: false
      })
      .string("-fm, --format-magic <string>", {
        desc: `Magic directive indicator (default: " =>")`
        // defaultValue: magicIndicator
      })
      .string("-fh, --format-heading <string>", {
        desc: `Delimiter for preserved comment blocks (default: "${STAR}")`
        // defaultValue: STAR
      })
      .string("-fo, --format-code-block-open <string>", {
        desc: 'Open code block string (default: "```js")'
        // defaultValue: codeFenceOpen
      })
      .string("-fc, --format-code-block-close <string>", {
        desc: 'Close code block string (default: "```")'
        // defaultValue: codeFenceClose
      })
      .outputSettings({ maxWidth: 75 })
      .parseAndExit()
      .catch(bad)
      .then(good)
  })
  return cli.map(program => {
    const config = Object.entries(program).reduce((o, [k, v]) => {
      const out = Object.assign(o, {
        [k]:
          (v === false || v === undefined) && defaultParamWorkaround[k]
            ? defaultParamWorkaround[k]
            : v
      })
      return out
    }, {})
    const {
      output,
      "no-meld": noMeld,
      "no-code": noCode,
      "print-config": printConfig,
      input: filename
    } = config
    if (printConfig) return console.log(config)
    config.code = !noCode
    let litContents
    try {
      litContents = literate(filename, config)
    } catch (e) {
      process.stderr.write("Error while literating -- " + e.message)
      return 1
    }
    if (!noMeld) {
      litContents = litContents.replace(/\n\n+/g, "\n\n")
    }
    if (output) {
      fs.writeFileSync(output, litContents, "utf8")
    } else {
      process.stdout.write(litContents)
    }
  })
}

export default ljs2
/* eslint-disable max-len */

/**
![ljs2](https://cdn.rawgit.com/brekk/ljs2/8a107c0/logo.svg)

```
      :::       ::::::::::: ::::::::   ::::::::
     :+:           :+:    :+:    :+: :+:    :+:
    +:+           +:+    +:+              +:+
   +#+           +#+    +#++:++#++     +#+
  +#+           +#+           +#+   +#+
 #+#       #+# #+#    #+#    #+#  #+#
########## #####      ########  ##########
```

> Generate docs from your source

## Command Line

If `ljs2` is installed globally, you can use `ljs2` command line tool to process your literate javascript files

You can use npx to proxy commands to ljs2 directly, e.g. `npx ljs2 input.js`

Otherwise, if you want it installed as a standalone, use `yarn global add ljs2` or `npm i -g ljs2`

```sh
$ ljs2 input-file.js -o output-file.md
$ ljs2 --help
```

*This file* is generated from ljs2's source code! [View it here](https://gitlab.com/brekk/ljs2/blob/master/src/ljs2.js)

## Usage

The basic idea with ljs2 is to prefix literate comments with the `/**` [literate comment block indicator](https://gitlab.com/brekk/ljs2/blob/master/src/ljs2.js#L113) (note the additional asterisk). Literate comments are then visible in the markdown document.

Additionally, individual, non-literate comments can use the magic directive: " =>" directly after the comment to either:

* via the `include` directive, include other literate code files: `// => include other-literate-file.js`
* via the `plain` directive, include other markdown files as included comments: `// => plain other-file.md`

## Configuration

Many of ljs2's behavior can be customized via flags. Please see `ljs2 --help` for more information, or [peruse the tests](https://gitlab.com/brekk/ljs2/blob/master/src/ljs2.spec.js) and their [fixtures](https://gitlab.com/brekk/ljs2/blob/master/src/fixtures)
 
*/
// => plain ../CHANGELOG.md
// => plain ../CONTRIBUTORS.md
// => plain ../related-work.md
// => plain ../LICENSE
/* eslint-disable max-len */
