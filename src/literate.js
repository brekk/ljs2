import fs from "fs"
import assert from "assert"
// import F from "fluture"

import { curry, forEach, find, map } from "ramda"

import { lex } from "./lex"
import { stripShebang, isWhitespace } from "./utils"

import { fileDirective } from "./directives"
import {
  regex,
  tokenTypes,
  NEWLINE,
  STAR,
  EMPTY,
  CODE,
  TEXT,
  codeFenceOpen,
  codeFenceClose,
  magicIndicator
} from "./constants"

export const commentHasMagicIndicator = (magic, t) => {
  if (t && t.type && t.value && t.value.value) {
    return (
      t.type === tokenTypes.comment &&
      t.value.type === tokenTypes.line &&
      t.value.value.slice(0, magic.length) === magic
    )
  }
  return false
}

export const getTokens = (magic, filename) => {
  const source = fs.readFileSync(filename, `utf8`)
  const raw = stripShebang(source)
  const tokens = lex({ sourceType: `module` }, raw)
  let resTokens = []
  forEach(t => {
    let r
    if (commentHasMagicIndicator(magic, t)) {
      const value = t.value.value.substr(magic.length + 1)
      r = fileDirective(filename, value, regex.plain, name => {
        resTokens.push({
          type: tokenTypes.plain,
          value: fs.readFileSync(name).toString()
        })
      })
      if (r) {
        return
      }
      r = fileDirective(filename, value, regex.include, name => {
        resTokens = resTokens.concat(getTokens(magic, name))
      })
      if (r) {
        return
      }
      assert(false, `unknown directive: ` + value)
    } else if (regex.ignore.test(t.value.value)) {
      // eslint-disable-next-line
      return
    } else {
      t.raw = raw.substr(t.range[0], t.range[1] - t.range[0])
      resTokens.push(t)
    }
  }, tokens)
  resTokens.push({
    type: tokenTypes.EOF,
    value: EMPTY
  })
  return resTokens
}
export const processLine = curry((indent, line) => {
  if (line.indexOf(indent) === 0) {
    return line.replace(indent, EMPTY)
  } else if (isWhitespace(line)) {
    return EMPTY
  } else {
    return line
  }
})

export const firstLine = lines => find(l => !isWhitespace(l), lines)
export const getIndent = lines => {
  const first = firstLine(lines)
  return first ? regex.whitespace.exec(first)[1] : EMPTY
}

export const unindent = value => {
  const lines = value.split(regex.newline)
  const indent = getIndent(lines)
  // Drop empty lines at the beginning of the literate comment
  while (lines[0] !== undefined && isWhitespace(lines[0])) {
    lines.shift()
  }
  // unindent lines
  return map(processLine(indent), lines).join(NEWLINE) + NEWLINE
}

export const typeIs = curry((x, { type }) => type === x)
export const isPlain = typeIs(tokenTypes.plain)
export const isEOF = typeIs(tokenTypes.EOF)
export const isCommentStart = (w, x) =>
  typeIs(tokenTypes.comment, x) &&
  x.value &&
  x.value.type === tokenTypes.block &&
  x.value.value &&
  x.value.value.indexOf(w) === 0

export const literate = (filename, opts = {}) => {
  const {
    code = false,
    "format-code-block-open": codeOpen = codeFenceOpen,
    "format-code-block-close": codeClose = codeFenceClose,
    "format-heading": formatHeading = STAR,
    "format-magic": formatMagic = magicIndicator
  } = opts
  const tokens = getTokens(formatMagic, filename)
  let state = CODE
  let content = EMPTY
  let codeBuffer = EMPTY
  const appendCode = () => {
    if (state === CODE) {
      state = TEXT
      if (!isWhitespace(codeBuffer)) {
        // console.log("codeOpen", codeOpen)
        content +=
          codeOpen +
          codeBuffer
            .replace(regex.strippable, EMPTY)
            .replace(regex.strippable2, EMPTY) +
          codeClose
      }
    }
  }
  const appendText = value => {
    if (content === EMPTY) {
      content = value
    } else {
      content += NEWLINE + value
    }
  }
  forEach(function processToken(t) {
    if (isPlain(t)) {
      appendCode()
      appendText(t.value)
    } else if (isEOF(t)) {
      appendCode()
      appendText(EMPTY)
    } else if (isCommentStart(formatHeading, t)) {
      appendCode()
      // literate comment
      const comment = t.value
      // block comment starting with /**
      const value = comment.value.slice(
        comment.value.indexOf(formatHeading) + formatHeading.length
      )
      appendText(unindent(value))
    } else if (code) {
      if (state !== CODE) {
        state = CODE
        codeBuffer = EMPTY
      }
      codeBuffer += t.raw
    }
  }, tokens)
  // code at the end of the file
  appendCode()
  // newline EOF
  content = content.replace(regex.EOF, NEWLINE)
  return content
}
export default literate
