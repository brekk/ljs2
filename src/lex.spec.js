import fs from "fs"
import path from "path"
import { keys } from "ramda"
import { lex } from "./lex"

test(`lex should generate a list of tokens`, () => {
  expect.assertions(2)
  const raw = lex(
    { sourceType: `module` },
    fs.readFileSync(
      path.resolve(__dirname, `./fixtures/fixture-array.js`),
      `utf8`
    )
  )
  // fs.writeFileSync(`./fixtures/fixture-array-lex.json`, JSON.stringify(raw), `utf8`)
  const rawLex = JSON.parse(
    fs.readFileSync(
      path.resolve(__dirname, `./fixtures/fixture-array-lex.json`),
      `utf8`
    )
  )
  expect(keys(raw)).toEqual(keys(rawLex))
  expect(raw).toEqual(rawLex)
})

// test(`lex should addWhitespace when raw.length !== currRange`, () => {
// })
