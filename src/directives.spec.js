import { fileDirective } from "./directives"
import { regex } from "./constants"

test(`fileDirective should do nothing when the match is not found`, () => {
  expect(typeof fileDirective).toEqual(`function`)
  expect(fileDirective(``, `xxxxxxxxxx`, `_`, () => {})).toBeFalsy()
})
test(`fileDirective should return success if a match was found`, () => {
  expect(
    fileDirective(
      __filename,
      `plain ./fixtures/base.md`,
      regex.whitespace,
      () => {}
    )
  ).toBeTruthy()
})
test(`fileDirective should barf with missing file`, () => {
  expect(() => {
    fileDirective(__filename, "plain ../missing-file.md", regex.plain, () => {})
  }).toThrow()
})
