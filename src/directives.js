import fs from "fs"
import path from "path"
import glob from "glob"
// import globFS from "glob-fs"
import isGlob from "is-glob"
// const globby = globFS()

/**
 * @function fileDirective
 * @param {string} filename - file path
 * @param {string} value - value for comparison
 * @param {regex} comparison - regular expression to match value
 * @param {function} callback - a function to call for every file match
 * @return {boolean} wasAnyThingMatched?
 */
export const fileDirective = function _fileDirective(
  filename,
  value,
  comparison,
  callback
) {
  const match = value.match(comparison)
  if (match) {
    const directivePattern = match[1]
    const globPattern = path.join(path.dirname(filename), directivePattern)
    const isGlobby = isGlob(globPattern)
    const files = isGlobby ? glob.sync(globPattern) : [globPattern]
    if (files.length === 0 || (!isGlobby && !fs.existsSync(globPattern))) {
      throw new Error(directivePattern + ` doesn't match any files`)
    }
    files.forEach(callback)
    return true
  } else {
    return false
  }
}
