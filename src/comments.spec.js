import { range, addWhitespace } from "./comments"

const commentObject = {
  type: `Comment`,
  value: {
    type: `Block`,
    value: [
      `*`,
      ` * @namespace util.array`,
      ` * @function join`,
      ` * @desc curried array.join with inverted parameter orders`,
      ` * @curried`,
      ` * @param {string} joiner - a string to join the array together with`,
      ` * @param {array} array`,
      ` * @return {string}`,
      ` * @example`,
      ` * <!--@join(\`x\`, [1,2,3])-->`,
      ` * <!--/@-->`,
      ` `
    ].join(`\n`),
    range: [33, 322],
    loc: {
      start: {
        line: 3,
        column: 0
      },
      end: {
        line: 14,
        column: 3
      }
    }
  },
  range: [33, 322],
  raw: [
    `/**`,
    ` * @namespace util.array`,
    ` * @function join`,
    ` * @desc curried array.join with inverted parameter orders`,
    ` * @curried`,
    ` * @param {string} joiner - a string to join the array together with`,
    ` * @param {array} array`,
    ` * @return {string}`,
    ` * @example`,
    ` * <!--@join(\`x\`, [1,2,3])-->`,
    ` * <!--/@-->`,
    ` */`
  ].join(`\n`)
}

test("range", () => {
  expect(() => range(0, 0, null)).toThrow()
  const comment = {
    range: [1, 2]
  }
  expect(range(0, 3, comment)).toBeTruthy()
  expect(range(1, 1, comment)).toBeFalsy()
})

test("addWhitespace", () => {
  expect(() => addWhitespace(null, null, null, null, null)).toThrow()
  const raw = "abcde"
  const syntax = {
    comments: [{ range: [0, 1] }]
  }
  const tokens = []
  const fromStart = 0
  const toEnd = 5
  expect(addWhitespace(raw, syntax, tokens, fromStart, toEnd)).toBeFalsy()
  expect(tokens).toEqual([
    { range: [0, 1], type: "Comment", value: { range: [0, 1] } },
    { range: [1, 5], type: "Whitespace", value: "bcde" }
  ])
})
test(`addWhitespace should throw if given a syntax input which lacks a comments node`, () => {
  expect(() => addWhitespace(`raw`, {}, [], 1, 1)).toThrow()
})
test(`range should return a boolean based on what (from, to) numbers were given`, () => {
  expect.assertions(3)
  expect(typeof range).toEqual(`function`)
  const from = 33
  const to = 322
  const comment = Object.assign({}, commentObject)
  const output = range(from, to, comment)
  expect(output).toBeTruthy()
  const output2 = range(20, 30, comment)
  expect(output2).toBeFalsy()
})
test(`range should throw if comment doesn't have range property`, () => {
  expect.assertions(1)
  expect(() => range(1, 2, {})).toThrow()
})
