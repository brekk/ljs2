import fs from "fs"
import path from "path"
import execa from "execa"
const relative = x => path.resolve(__dirname, x)
const executable = relative("../ljs2.js")
const hasFile = fs.existsSync(executable)
const testify = hasFile ? test : test.skip

const good = ({ stdout }) => stdout.split("\n")
const bad = ({ stderr }) => stderr

const run = x => execa(executable, x)

const testName = x => (hasFile ? x : "./ljs2.js -- NO EXECUTABLE FOUND")

testify(testName("ljs2 --help"), () => {
  const expected = [
    "",
    "      :::       ::::::::::: ::::::::   ::::::::",
    "     :+:           :+:    :+:    :+: :+:    :+:",
    "    +:+           +:+    +:+              +:+",
    "   +#+           +#+    +#++:++#++     +#+",
    "  +#+           +#+           +#+   +#+",
    " #+#       #+# #+#    #+#    #+#  #+#",
    "########## #####      ########  ##########",
    "",
    "a tool for working with literate JS",
    "",
    "Usage: ljs2 [input:string] [options]",
    "",
    "Arguments:",
    "  [input:string]  The input file                                   [string]",
    "",
    "Options:",
    "  -h, --help                               Show help",
    "                                           [commands: help] [boolean]",
    "",
    "  -v, --version                            Show version number",
    "                                           [commands: version] [boolean]",
    "",
    "  -o, --output                             An output file",
    "                                           [string]",
    "",
    "  -C, --no-code                            Don't include code in the",
    "                                           output file",
    "                                           [boolean]",
    "",
    "  -p, --print-config                       Print the existing",
    "                                           configuration and exit",
    "                                           [boolean]",
    "",
    "  -M, --no-meld                            Don't meld consecutive",
    "                                           line-breaks",
    "                                           [boolean]",
    "",
    "  -fm, --format-magic <string>             Magic directive indicator",
    '                                           (default: " =>")',
    "                                           [string]",
    "",
    "  -fh, --format-heading <string>           Delimiter for preserved comment",
    '                                           blocks (default: "*")',
    "                                           [string]",
    "",
    "  -fo, --format-code-block-open <string>   Open code block string",
    '                                           (default: "```js")',
    "                                           [string]",
    "",
    "  -fc, --format-code-block-close <string>  Close code block string",
    '                                           (default: "```")',
    "                                           [string]"
  ]
  return expect(
    run(["--help"])
      .then(good)
      .catch(bad)
  ).resolves.toEqual(expected)
})

testify(testName(`ljs2 /src/fixtures/base.js`), () => {
  const expected = ["", "```js", "const y = 2", "```"]
  return expect(
    run([relative("./fixtures/base.js")]).then(good)
  ).resolves.toEqual(expected)
})

testify(testName(`ljs2 /src/fixtures/plain-directive.js`), () => {
  const expected = [
    "# hello, this is a base fixture",
    "```",
    "const ljs2 = 'cool'",
    "```"
  ]
  return expect(
    run([relative("./fixtures/plain-directive.js")])
      .then(good)
      .catch(bad)
  ).resolves.toEqual(expected)
})
testify(testName("ljs -p"), () => {
  const expected = [
    "{ _: [],",
    "  h: false,",
    "  help: false,",
    "  v: false,",
    "  version: false,",
    "  input: undefined,",
    "  o: undefined,",
    "  output: undefined,",
    "  C: false,",
    "  'no-code': false,",
    "  p: true,",
    "  'print-config': true,",
    "  M: false,",
    "  'no-meld': false,",
    "  fm: ' =>',",
    "  'format-magic': ' =>',",
    "  fh: '*',",
    "  'format-heading': '*',",
    "  fo: '\\n```js\\n',",
    "  'format-code-block-open': '\\n```js\\n',",
    "  fc: '\\n```\\n\\n',",
    "  'format-code-block-close': '\\n```\\n\\n' }"
  ]
  return expect(run(["-p"]).then(good)).resolves.toEqual(expected)
})

testify(testName(`ljs2 /src/fixtures/no-code.js --no-code`), () => {
  const expected = ["# h1", "## h2", "### h3", "ljs2 is *cool*."]
  return expect(
    run([relative("./fixtures/no-code.js"), "--no-code"]).then(good)
  ).resolves.toEqual(expected)
})

testify(testName(`ljs2 /src/fixtures/no-code-recursive.js --no-code`), () => {
  const expected = [
    "cool cool cool cool cool",
    "",
    "# h1",
    "## h2",
    "### h3",
    "ljs2 is *cool*."
  ]
  return expect(
    run([relative("./fixtures/no-code-recursive.js"), "--no-code"]).then(good)
  ).resolves.toEqual(expected)
})

testify(testName(`ljs2 /src/fixtures/bad-directive.js`), () => {
  const expected =
    "Error while literating -- unknown directive: bad-directive ../nearby-file.md"
  const asynchro = run([relative("./fixtures/bad-directive.js")]).then(bad)
  return expect(asynchro).resolves.toEqual(expected)
})

testify(testName(`ljs2 /src/fixtures/include-directive.js`), () => {
  const expected = [
    "",
    "```js",
    "// cool test",
    "const y = 2",
    "```",
    "",
    "```js",
    "const x = 2",
    "```"
  ]
  const asynchro = run([relative("./fixtures/include-directive.js")]).then(good)
  return expect(asynchro).resolves.toEqual(expected)
})

testify(testName(`ljs2 /src/fixtures/include-glob.js`), () => {
  const expected = [
    "",
    "```js",
    "const b = 10",
    "```",
    "",
    "```js",
    "const c = 15",
    "```",
    "",
    "```js",
    "const a = 5",
    "```"
  ]
  const asynchro = run([relative("./fixtures/include-glob.js")]).then(good)
  return expect(asynchro).resolves.toEqual(expected)
})

testify(testName(`ljs2 /src/fixtures/include-glob.js --no-meld`), () => {
  const expected = [
    "",
    "```js",
    "const b = 10",
    "```",
    "",
    "",
    "",
    "```js",
    "const c = 15",
    "```",
    "",
    "",
    "",
    "```js",
    "const a = 5",
    "```"
  ]
  const asynchro = run([
    relative("./fixtures/include-glob.js"),
    "--no-meld"
  ]).then(good)
  return expect(asynchro).resolves.toEqual(expected)
})

testify(testName(`ljs2 -p -fh="@@@"`), () => {
  const expected = [
    "{ _: [],",
    "  h: false,",
    "  help: false,",
    "  v: false,",
    "  version: false,",
    "  input: undefined,",
    "  o: undefined,",
    "  output: undefined,",
    "  C: false,",
    "  'no-code': false,",
    "  p: true,",
    "  'print-config': true,",
    "  M: false,",
    "  'no-meld': false,",
    "  fm: ' =>',",
    "  'format-magic': ' =>',",
    "  fh: '@@@',",
    "  'format-heading': '@@@',",
    "  fo: '\\n```js\\n',",
    "  'format-code-block-open': '\\n```js\\n',",
    "  fc: '\\n```\\n\\n',",
    "  'format-code-block-close': '\\n```\\n\\n' }"
  ]
  const asynchro = run(["-p", "--format-heading=@@@"]).then(good)
  return expect(asynchro).resolves.toEqual(expected)
})
testify(testName(`ljs2 ./fixtures/regular-format.js`), () => {
  const expected = [
    "COOL HEADER COOL STUFF",
    "",
    "```js",
    "const code = `",
    "  the files are in the computer",
    "`",
    "```"
  ]
  const asynchro = run([relative("./fixtures/regular-format.js")]).then(good)
  return expect(asynchro).resolves.toEqual(expected)
})
testify(testName(`ljs2 ./fixtures/alternate-format.js -fh="@@@"`), () => {
  const expected = [
    "COOL HEADER COOL STUFF",
    "",
    "```js",
    "const code = `",
    "  the files are in the computer",
    "`",
    "```"
  ]
  const asynchro = run([
    relative("./fixtures/alternate-format.js"),
    "--format-heading=@@@"
  ]).then(good)
  return expect(asynchro).resolves.toEqual(expected)
})
testify(testName('ljs2 --fo="```butts" -p'), () => {
  const expected = [
    "{ _: [],",
    "  h: false,",
    "  help: false,",
    "  v: false,",
    "  version: false,",
    "  input: undefined,",
    "  o: undefined,",
    "  output: undefined,",
    "  C: false,",
    "  'no-code': false,",
    "  p: true,",
    "  'print-config': true,",
    "  M: false,",
    "  'no-meld': false,",
    "  fm: ' =>',",
    "  'format-magic': ' =>',",
    "  fh: '*',",
    "  'format-heading': '*',",
    "  fo: '\\n```butts\\n',",
    "  'format-code-block-open': '\\n```butts\\n',",
    "  fc: '\\n```\\n\\n',",
    "  'format-code-block-close': '\\n```\\n\\n' }"
  ]
  const asynchro = run(["-p", "--fo=\n```butts\n"]).then(good)
  return expect(asynchro).resolves.toEqual(expected)
})

testify(testName('ljs2 ./fixtures/regular-format.js --fo="```butts"'), () => {
  const expected = [
    "COOL HEADER COOL STUFF",
    "",
    "```butts",
    "const code = `",
    "  the files are in the computer",
    "`",
    "```"
  ]
  const asynchro = run([
    relative("./fixtures/regular-format.js"),
    "--fo=```butts\n"
  ]).then(good)
  return expect(asynchro).resolves.toEqual(expected)
})

testify(testName("ljs2 ./fixtures/magic-indicator.js --fm=%%%"), () => {
  const expected = [
    "Here's some stuff.",
    "",
    "# hello, this is a base fixture",
    "```",
    "const ljs2 = 'cool'",
    "```"
  ]

  const asynchro = run([
    relative("./fixtures/magic-indicator.js"),
    "--fm=%%%"
  ]).then(good)
  return expect(asynchro).resolves.toEqual(expected)
})

testify(testName("ljs2 --fm=%%% -p"), () => {
  const expected = [
    "{ _: [],",
    "  h: false,",
    "  help: false,",
    "  v: false,",
    "  version: false,",
    "  input: undefined,",
    "  o: undefined,",
    "  output: undefined,",
    "  C: false,",
    "  'no-code': false,",
    "  p: true,",
    "  'print-config': true,",
    "  M: false,",
    "  'no-meld': false,",
    "  fm: '%%%',",
    "  'format-magic': '%%%',",
    "  fh: '*',",
    "  'format-heading': '*',",
    "  fo: '\\n```js\\n',",
    "  'format-code-block-open': '\\n```js\\n',",
    "  fc: '\\n```\\n\\n',",
    "  'format-code-block-close': '\\n```\\n\\n' }"
  ]
  const asynchro = run(["--fm=%%%", "-p"]).then(good)
  return expect(asynchro).resolves.toEqual(expected)
})
