import fs from "fs"
import path from "path"
import R from "ramda"
import {
  typeIs,
  isCommentStart,
  getIndent,
  firstLine,
  processLine,
  getTokens,
  unindent,
  literate
} from "../src/literate"
import { magicIndicator, STAR } from "./constants"
import { stripShebang, isWhitespace } from "./utils"
const relative = x => path.resolve(__dirname, x)

test(`isWhitespace will verify whether a given string is whitespace`, () => {
  expect.assertions(3)
  expect(typeof isWhitespace).toEqual(`function`)
  expect(isWhitespace(`   `)).toBeTruthy()
  expect(isWhitespace(`nope`)).toBeFalsy()
})
test(`stripShebang will remove a line which matches a shebang, i.e. '#!/usr/bin/env node'`, () => {
  expect.assertions(3)
  expect(typeof stripShebang).toEqual(`function`)
  expect(stripShebang(`#!/usr/bin/env node\n`)).toEqual(``)
  expect(stripShebang(`/usr/bin/env node\n`)).toEqual(`/usr/bin/env node\n`)
})

test(`getTokens should generate tokens for a given file`, () => {
  expect.assertions(2)
  expect(typeof getTokens).toEqual(`function`)
  const tokenized = getTokens(
    magicIndicator,
    relative(`./fixtures/fixture-array.js`)
  )
  const jsonFixture = relative(`./fixtures/fixture-array-tokens.json`)
  // (() => {
  //   fs.writeFileSync(jsonFixture, JSON.stringify(tokenized), `utf8`)
  // })()
  expect(tokenized).toEqual(JSON.parse(fs.readFileSync(jsonFixture, "utf8")))
})

test(`getTokens should ignore lines with the # directive`, () => {
  const tokenized = getTokens(
    magicIndicator,
    relative(`./fixtures/fixture-array.js`)
  )
  const iterator = R.pipe(
    R.map(R.path([`value`, `value`])),
    R.filter(R.identity),
    R.filter(x => x.indexOf(`visible`) > -1),
    R.length
  )
  const linesThatSayVisible = iterator(tokenized)
  expect(linesThatSayVisible).toEqual(1)
})
test(`processLine should do stuff based on indent and a given line`, () => {
  expect(processLine("blah", "")).toEqual("")
})
test(`unindent should unindent lines which are indented`, () => {
  expect.assertions(2)
  expect(typeof unindent).toEqual(`function`)
  const out = unindent(
    [``, `    0`, `    1`, `    2`, `  3  `, `    4    `].join(`\n`)
  )
  expect(out).toEqual(`0\n1\n2\n  3  \n4    \n`)
})
test(`getTokens, unknown directive`, () => {
  expect(() =>
    getTokens(magicIndicator, relative("./fixtures/bad-directive.js"))
  ).toThrow()
})
test("getTokens, plain directive", () => {
  const x = getTokens(magicIndicator, relative("./fixtures/plain-directive.js"))
  expect(x).toEqual([
    {
      type: "Plain",
      value: "# hello, this is a base fixture\n```\nconst ljs2 = 'cool'\n```\n"
    },
    {
      type: "Whitespace",
      value: "\n",
      range: [21, 22],
      raw: "\n"
    },
    {
      type: "EOF",
      value: ""
    }
  ])
})
test(`getTokens, include directive`, () => {
  const x = getTokens(
    magicIndicator,
    relative("./fixtures/include-directive.js")
  )
  expect(x).toEqual([
    {
      range: [0, 12],
      raw: "// cool test",
      type: "Comment",
      value: {
        loc: { end: { column: 12, line: 1 }, start: { column: 0, line: 1 } },
        range: [0, 12],
        type: "Line",
        value: " cool test"
      }
    },
    { range: [12, 13], raw: "\n", type: "Whitespace", value: "\n" },
    {
      loc: { end: { column: 5, line: 1 }, start: { column: 0, line: 1 } },
      range: [0, 5],
      raw: "const",
      type: "Keyword",
      value: "const"
    },
    { range: [5, 6], raw: " ", type: "Whitespace", value: " " },
    {
      loc: { end: { column: 7, line: 1 }, start: { column: 6, line: 1 } },
      range: [6, 7],
      raw: "y",
      type: "Identifier",
      value: "y"
    },
    { range: [7, 8], raw: " ", type: "Whitespace", value: " " },
    {
      loc: { end: { column: 9, line: 1 }, start: { column: 8, line: 1 } },
      range: [8, 9],
      raw: "=",
      type: "Punctuator",
      value: "="
    },
    { range: [9, 10], raw: " ", type: "Whitespace", value: " " },
    {
      loc: { end: { column: 11, line: 1 }, start: { column: 10, line: 1 } },
      range: [10, 11],
      raw: "2",
      type: "Numeric",
      value: "2"
    },
    { range: [11, 12], raw: "\n", type: "Whitespace", value: "\n" },
    { type: "EOF", value: "" },
    { range: [34, 35], raw: "\n", type: "Whitespace", value: "\n" },
    {
      loc: { end: { column: 5, line: 3 }, start: { column: 0, line: 3 } },
      range: [35, 40],
      raw: "const",
      type: "Keyword",
      value: "const"
    },
    { range: [40, 41], raw: " ", type: "Whitespace", value: " " },
    {
      loc: { end: { column: 7, line: 3 }, start: { column: 6, line: 3 } },
      range: [41, 42],
      raw: "x",
      type: "Identifier",
      value: "x"
    },
    { range: [42, 43], raw: " ", type: "Whitespace", value: " " },
    {
      loc: { end: { column: 9, line: 3 }, start: { column: 8, line: 3 } },
      range: [43, 44],
      raw: "=",
      type: "Punctuator",
      value: "="
    },
    { range: [44, 45], raw: " ", type: "Whitespace", value: " " },
    {
      loc: { end: { column: 11, line: 3 }, start: { column: 10, line: 3 } },
      range: [45, 46],
      raw: "2",
      type: "Numeric",
      value: "2"
    },
    { range: [46, 47], raw: "\n", type: "Whitespace", value: "\n" },
    { type: "EOF", value: "" }
  ])
})
test(`literate`, () => {
  expect.assertions(2)
  expect(typeof literate).toEqual(`function`)
  const output = literate(relative(`./fixtures/fixture-array.js`), {
    code: true
  })
  // fs.writeFileSync(`./fixtures/fixture-array-literate.md`, output, `utf8`)
  const expected = fs.readFileSync(
    relative(`./fixtures/fixture-array-literate.md`),
    `utf8`
  )
  expect(expected).toEqual(output)
})
test(`literate with empty`, () => {
  const x = literate(relative("./fixtures/plain-directive.js"), { code: false })
  expect(x).toEqual(
    "# hello, this is a base fixture\n```\nconst ljs2 = 'cool'\n```\n"
  )
})
test(`literate with no opts`, () => {
  const x = literate(relative("./fixtures/plain-directive.js"))
  expect(x).toEqual(
    "# hello, this is a base fixture\n```\nconst ljs2 = 'cool'\n```\n"
  )
})
test(`firstLine`, () => {
  const input = ["   ", "   ", "butts"]
  expect(firstLine(input)).toEqual("butts")
})
test(`getIndent should return the indent based on an array of lines`, () => {
  expect(getIndent(["    ", "  cool", "  "])).toEqual("  ")
  expect(getIndent([" ", " ", " "])).toEqual("")
})

test(`literate with globs`, () => {
  const x = literate(relative("./fixtures/include-glob.js"), { code: true })
  expect(x.split("\n")).toEqual([
    "",
    "```js",
    "const b = 10",
    "```",
    "",
    "",
    "",
    "```js",
    "const c = 15",
    "```",
    "",
    "",
    "",
    "```js",
    "const a = 5",
    "```",
    ""
  ])
})

test(`typeIs`, () => {
  expect(typeIs("butts", { type: "butts" })).toBeTruthy()
})
test(`isCommentStart`, () => {
  expect(
    isCommentStart(STAR, {
      type: "Comment",
      value: { type: "Block", value: "209302982" }
    })
  ).toBeFalsy()
  expect(
    isCommentStart(STAR, {
      type: `Comment`,
      value: { type: "Block", value: "*" }
    })
  ).toEqual(true)
})
